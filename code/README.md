Interface de visualisation de l'état physiologique d'un pilote d'avion de chasse
=============

Le but de ce projet est de développer une interface web permettant de visualiser les différents signaux PPG ainsi que de pouvoir voir les indicateurs calculer grâce à la pipeline déjà existante en python. L'interface permettra d'ajouter de nouvelles données de vol et des experts en physiologie pourront utiliser cette interface afin de visualiser les données du vol.


Prérequis
---------

- Python 3.7
- NodeJS


Installation
------------

Pour installer les dépendances :
    cd frontend/
    npm install


Lancement
---------

Pour lancer l'API 
    cd backend/
    python backend.py

Pour lancer le frontend
    cd frontend/
    npm start
