import React, { useContext, useEffect, useState } from 'react';
import { Container, Accordion, Row, Col, Button, Card, useAccordionButton, AccordionContext } from 'react-bootstrap';
import FlightList from './FlightList';

const PilotList = (props) => {
	const [pilots, setPilots] = useState([]);
	const [loaded, setLoaded] = useState(false);

	useEffect(() => {
		fetch('http://localhost:8080/')
			.then((res) => res.json())
			.then((json) => {
				const plts = JSON.parse(JSON.stringify(json));
				const plts_array = [];
				plts.forEach((plt) => {
					const flights_array = [];
					let plt_json = JSON.parse(plt);
					plt_json.flights[0].forEach((flight) => {
						flights_array.push(JSON.parse(flight));
					});
					plt_json.flights = flights_array;
					plts_array.push(plt_json);
				});
				setPilots(plts_array);
				setLoaded(true);
			});
	}, []);

	function handleClick() {
		console.log('click');
	}

	function CustomToggle({ children, eventKey, callback }) {
		const { activeEventKey } = useContext(AccordionContext);
		const decoratedOnClick = useAccordionButton(eventKey, () => callback && callback(eventKey));
		const isCurrentEventKey = activeEventKey === eventKey;
		if (isCurrentEventKey) {
			return (
				<Card.Body onClick={decoratedOnClick} style={{ cursor: 'pointer', display: 'block' }} className='accordion-button'>
					{children}
				</Card.Body>
			);
		} else {
			return (
				<Card.Body onClick={decoratedOnClick} style={{ cursor: 'pointer', display: 'block' }} className='accordion-button collapsed'>
					{children}
				</Card.Body>
			);
		}
	}

	if (!loaded) return <Container>Loading</Container>;
	return (
		<Container className='mt-3' style={{ userSelect: 'none' }}>
			<h1>Liste des pilotes</h1>
			<Accordion>
				<Card className='rounded-0'>
					<Card.Header>
						<Row className='align-items-center'>
							<Col xs={3}>
								<strong>Nom</strong>
							</Col>
							<Col xs={3}>
								<strong>Date anniversaire</strong>
							</Col>
							<Col xs={2}>
								<strong>Taille (cm)</strong>
							</Col>
							<Col xs={2}>
								<strong>Poids (kg)</strong>
							</Col>
							<Col xs={2}>
								<Button onClick={handleClick}>Ajouter un vol</Button>
							</Col>
						</Row>
					</Card.Header>
				</Card>
				{pilots.map((pilot) => (
					<Card className='rounded-0' key={pilot.pilot_id}>
						<CustomToggle eventKey={pilot.pilot_id}>
							<Row>
								<Col xs={3}>
									{pilot.lastname} {pilot.firstname}
								</Col>
								<Col xs={3}>{pilot.birthday}</Col>
								<Col xs={2}>{pilot.height}cm</Col>
								<Col xs={2}>{pilot.weight}kg</Col>
							</Row>
						</CustomToggle>
						<Accordion.Collapse eventKey={pilot.pilot_id}>
							<FlightList pilot={pilot} flights={pilot.flights} />
						</Accordion.Collapse>
					</Card>
				))}
			</Accordion>
		</Container>
	);
};

export default PilotList;
