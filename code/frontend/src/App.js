import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import PilotList from './PilotList';
import FlightDetails from './FlightDetails';

const App = (props) => {
	return (
		<Router>
			<Routes>
				<Route exact path='/' element={<PilotList />} />
				<Route path='/flight' element={<FlightDetails />} />
			</Routes>
		</Router>
	);
};

export default App;
