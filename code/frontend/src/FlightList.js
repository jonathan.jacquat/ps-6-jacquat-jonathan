import React from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

const FlightList = (props) => {
	const nav = useNavigate();

	function handleFlight(flight) {
		nav('/flight', {
			state: { flight: flight, pilot: props.pilot },
		});
	}

	return (
		<div className='m-3'>
			<h3 className='mb-3'>Liste des vols</h3>
			<Card className='rounded-0'>
				<Card.Header>
					<Row>
						<Col>
							<strong>Date du vol</strong>
						</Col>
						<Col>
							<strong>Lieu départ</strong>
						</Col>
						<Col>
							<strong>Lieu arrivée</strong>
						</Col>
					</Row>
				</Card.Header>
			</Card>
			{props.flights.map((flight) => (
				<Card className='rounded-0' key={flight.flight_id} onClick={(e) => handleFlight(flight)}>
					<Card.Body>
						<Row>
							<Col>{flight.flight_date}</Col>
							<Col>{flight.city_departure}</Col>
							<Col>{flight.city_arrival}</Col>
						</Row>
					</Card.Body>
				</Card>
			))}
		</div>
	);
};

export default FlightList;
