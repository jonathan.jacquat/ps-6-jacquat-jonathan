import React, { useState, useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { Container, Row, Col, Button, Form, Table } from 'react-bootstrap';
import Plotly from 'plotly.js-basic-dist';
import createPlotlyComponent from 'react-plotly.js/factory';

const FlightDetails = (props) => {
	const { state } = useLocation();
	const [flight] = useState(state.flight);
	const [pilot] = useState(state.pilot);
	const [annotation, setAnnotation] = useState(flight.annotations);
	const [features, setFeatures] = useState([]);
	const [rawData, setRawData] = useState([]);
	const [cleanedData, setCleanedData] = useState([]);
	const [rawDataLength, setRawDataLength] = useState([]);
	const [cleanedDataLength, setCleanedDataLength] = useState([]);
	const [timestampStart, setTimestampStart] = useState(0);
	const [timestampEnd, setTimestampEnd] = useState(0);
	const nav = useNavigate();
	const Plot = createPlotlyComponent(Plotly);

	useEffect(() => {
		prepareData();
	}, []);

	function prepareData() {
		var prepareData = JSON.parse(window.atob(flight.data_csv));
		prepareData.splice(0, 2);
		var array = [];
		for (let i = 0; i < prepareData.length; i++) {
			array.push(i / 64);
		}
		setRawData(prepareData);
		setRawDataLength(array);
		setTimestampEnd(parseInt(array.at(-1) * 1000));
	}

	function handleBack() {
		nav(-1);
	}

	function handleExportation() {
		console.log('exportation');
	}

	function handleAnnotationChange(event) {
		console.log(event.target.value);
		setAnnotation(event.target.value);
	}

	function handleTimestampStartChange(event) {
		setTimestampStart(event.target.value);
	}

	function handleTimestampEndChange(event) {
		setTimestampEnd(event.target.value);
	}

	function handleIndicators() {
		const start = Math.round((timestampStart / 1000) * 64);
		const end = Math.round((timestampEnd / 1000) * 64);
		fetch('http://localhost:8080/pipeline?flight_id=' + flight.flight_id + '&start=' + start + '&end=' + end)
			.then((res) => res.json())
			.then((json) => {
				const processedData = json[0];
				const features = json[1];
				const parsedProcessedData = JSON.parse(processedData);
				const fts = JSON.parse(features);
				let ftsArray = [];
				for (var i = 0; i < fts['columns'].length; i++) {
					ftsArray.push({ name: fts['columns'][i], value: fts['data'][0][i] === null ? '-' : fts['data'][0][i] });
				}
				setFeatures(ftsArray);
				setCleanedData(parsedProcessedData);
				let array = [];
				for (i = start; i < parsedProcessedData.length + start; i++) {
					array.push(i / 64);
				}
				setCleanedDataLength(array);
			});
	}

	const exportButton = () => {
		if (features.length > 0) {
			return (
				<Button variant='outline-success' onClick={handleExportation} style={{ width: '100%' }}>
					Exporter
				</Button>
			);
		} else {
			return (
				<Button variant='outline-success' disabled onClick={handleExportation} style={{ width: '100%' }}>
					Exporter
				</Button>
			);
		}
	};

	return (
		<Container className='mt-3'>
			<Row className='mb-1'>
				<Col xxl='6' className='me-2'>
					<Row>
						<Col sm='2' className='mt-2'>
							<Button onClick={handleBack} variant='outline-secondary'>
								Retour
							</Button>
						</Col>
						<Col>
							<h2>Informations générales</h2>
							<Table borderless size='sm'>
								<tbody>
									<tr>
										<td>
											<b>Date:</b> {flight.flight_date}
										</td>
										<td>
											<b>Nom:</b> {pilot.lastname} {pilot.firstname}
										</td>
									</tr>
									<tr>
										<td>
											<b>Départ:</b> {flight.city_departure}
										</td>
										<td>
											<b>Date anniversaire:</b> {pilot.birthday}
										</td>
									</tr>
									<tr>
										<td>
											<b>Arrivée:</b> {flight.city_arrival}
										</td>
										<td>
											<b>Poids:</b> {pilot.height}cm
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<b>Taille:</b> {pilot.weight} kg
										</td>
									</tr>
								</tbody>
							</Table>
						</Col>
					</Row>
				</Col>
				<Col>
					<Row>
						<h2>Annotations</h2>
						<Form.Control as='textarea' name='date' onChange={handleAnnotationChange} placeholder='Annotations...' rows={5} value={annotation} />
					</Row>
				</Col>
			</Row>
			<Row className='mb-3'>
				<h2>Affichage du signal</h2>
				{cleanedData.length > 0 && (
					<Plot
						data={[
							{
								x: rawDataLength,
								y: rawData,
								name: 'Raw signal',
								type: 'scatter',
								mode: 'lines',
								line: { color: '#17BECF' },
							},
							{
								x: cleanedDataLength,
								y: cleanedData,
								name: 'Cleaned signal',
								mode: 'lines',
							},
						]}
						layout={{
							autosize: true,
							xaxis: {
								autorange: true,
								type: 'linear',
							},
							yaxis: {
								autorange: true,
								type: 'linear',
							},
						}}
						useResizeHandler={true}
						style={{ width: '100%', height: '100%' }}
					/>
				)}
				{cleanedData.length === 0 && (
					<Plot
						data={[
							{
								x: rawDataLength,
								y: rawData,
								name: 'Raw signal',
								type: 'scatter',
								mode: 'lines',
								line: { color: '#17BECF' },
							},
						]}
						layout={{
							autosize: true,
							xaxis: {
								autorange: true,
								type: 'linear',
							},
							yaxis: {
								autorange: true,
								type: 'linear',
							},
						}}
						useResizeHandler={true}
						style={{ width: '100%', height: '100%' }}
					/>
				)}
			</Row>
			<Row className='mb-3'>
				<h2>Calcul des indicateurs</h2>
				<Col>
					<Form.Control name='tsStart' placeholder='Timestamp début' defaultValue={0} onChange={handleTimestampStartChange}>
						{flight.ts_start}
					</Form.Control>
				</Col>
				<Col>
					<Form.Control name='tsEnd' placeholder='Timestamp fin' value={timestampEnd} onChange={handleTimestampEndChange}>
						{flight.ts_end}
					</Form.Control>
				</Col>
				<Col>
					<Button variant='outline-primary' style={{ width: '100%' }} onClick={handleIndicators}>
						Caluler indicateurs
					</Button>
				</Col>
				<Col>{exportButton()}</Col>
				<Col>
					<Button style={{ width: '100%' }} disabled variant='outline-dark'>
						Liste des exportations
					</Button>
				</Col>
			</Row>
			<Row className='mb-3'>
				<h3>Indicateurs</h3>
				<Col>
					<Table hover size='sm' striped responsive='sm'>
						<thead className='thead-dark'>
							<tr>
								<th style={{ width: '50%' }}>Indicateurs</th>
								<th style={{ width: '50%' }}>Valeur</th>
							</tr>
						</thead>
						<tbody>
							{features.length > 0 &&
								features.map((feature) => (
									<tr key={feature.name}>
										<td>{feature.name}</td>
										<td>{feature.value}</td>
									</tr>
								))}
						</tbody>
					</Table>
				</Col>
			</Row>
		</Container>
	);
};

export default FlightDetails;
