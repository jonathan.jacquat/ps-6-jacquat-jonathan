import numpy as np
import base64

PATH = "data/"


def save_base64(filename):
    arr = np.loadtxt(PATH + filename, delimiter='\n')
    print(arr)
    array_str = '['
    for i in range(0, len(arr)):
        if i != len(arr)-1:
            array_str += str(arr[i]) + ','
        else:
            array_str += str(arr[i]) + ']'

    array_str = array_str
    array_bytes = array_str.encode('utf-8')
    b64_bytes = base64.b64encode(array_bytes)
    return b64_bytes.decode('utf-8')


if __name__ == '__main__':
    save_base64("stairs.csv")
