import json
import datetime

import files
import save

import cherrypy
import cherrypy_cors
from sqlalchemy import create_engine, text
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date, Text, ForeignKey

import pipeline

DB_PATH = '/db/db_ps6.db'

DATE_FORMAT_DB = "%Y-%m-%d"
DATE_FORMAT_FRONT = "%d.%m.%Y"

engine = create_engine("sqlite+pysqlite://" + DB_PATH, echo=False)
Session = sessionmaker(bind=engine)

Base = declarative_base()


class Main(object):

    @cherrypy.expose()
    @cherrypy.tools.json_out()
    def index(self):
        pilots = []
        with Session() as session:
            for pilot in session.query(Pilot):
                pilots.append(pilot.to_json())
        return pilots

    @cherrypy.expose()
    def flight(self, flight_id):
        with Session() as session:
            flight = session.query(Flight).get(flight_id)
            return flight.to_json()

    @cherrypy.expose()
    @cherrypy.tools.json_out()
    def pipeline(self, flight_id, start, end):
        with Session() as session:
            flight = session.query(Flight).get(flight_id)
            ppg_clean, features = pipeline.get_features(flight.data_csv, start, end)
            return [ppg_clean.to_json(orient="values"), features.to_json(orient="split")]

    @cherrypy.expose()
    def init(self):
        with engine.connect() as conn:
            conn.execute(text("DROP TABLE pilot"))
            conn.execute(text("DROP TABLE flight"))
            conn.execute(text("DROP TABLE exportation"))
        Base.metadata.create_all(engine)
        with Session() as session:
            session.add(Pilot(lastname="Jacquat", firstname="Jonathan",
                              birthday=datetime.datetime.strptime("1999-01-22", DATE_FORMAT_DB).date(), height=178,
                              weight=63))
            session.add(Pilot(lastname="Meteier", firstname="Quentin",
                              birthday=datetime.datetime.strptime("1996-08-15", DATE_FORMAT_DB).date(), height=180,
                              weight=67))
            session.add(Pilot(lastname="Abou Khaled", firstname="Omar",
                              birthday=datetime.datetime.strptime("1970-10-03", DATE_FORMAT_DB).date(), height=168,
                              weight=60))
            session.add(Flight(flight_date=datetime.datetime.strptime("2022-03-29", DATE_FORMAT_DB).date(),
                               city_departure="Payerne", city_arrival="Sion", data_csv=save.save_base64("all.csv"),
                               annotations="Données de l'activité : cycling", pilot_id=1))
            session.add(Flight(flight_date=datetime.datetime.strptime("2022-02-13", DATE_FORMAT_DB).date(),
                               city_departure="Paris", city_arrival="Genève", data_csv=save.save_base64("driving.csv"),
                               annotations="Ces données ont été prises lors de la conduite d'une voiture", pilot_id=1))
            session.add(Flight(flight_date=datetime.datetime.strptime("2022-03-16", DATE_FORMAT_DB).date(),
                               city_departure="Genève", city_arrival="Londres", data_csv=save.save_base64("soccer.csv"),
                               annotations="Un joueur de football a porté une montre pour récupérer ces données",
                               pilot_id=2))
            session.add(Flight(flight_date=datetime.datetime.strptime("2022-03-27", DATE_FORMAT_DB).date(),
                               city_departure="Istanbul", city_arrival="Le Caire", data_csv=save.save_base64("stairs.csv"),
                               annotations="C'est parfois long de monter des escaliers", pilot_id=2))
            session.add(Flight(flight_date=datetime.datetime.strptime("2022-03-02", DATE_FORMAT_DB).date(),
                               city_departure="Payerne", city_arrival="Payerne", data_csv=save.save_base64("baseline.csv"),
                               annotations="Promenade autours du lac de la Gruyère", pilot_id=3))
            session.commit()


class Pilot(Base):
    __tablename__ = 'pilot'

    pilot_id = Column(Integer, primary_key=True)
    lastname = Column(String(50))
    firstname = Column(String(50))
    birthday = Column(Date)
    height = Column(Integer)
    weight = Column(Integer)
    flights = relationship("Flight", back_populates="pilot")

    def __init__(self, lastname, firstname, birthday, height, weight):
        self.lastname = lastname
        self.firstname = firstname
        self.birthday = birthday
        self.height = height
        self.weight = weight

    def __str__(self):
        return str(self.pilot_id) + ", " + self.lastname + ", " + self.firstname + ", " + self.birthday.strftime(DATE_FORMAT_FRONT) + ", " + str(self.height) + ", " + str(self.weight)

    def to_json(self):
        flights_json = []
        for flight in self.flights:
            flights_json.append(flight.to_json())
        json_string = '{' + '"pilot_id": ' + str(self.pilot_id) + ', "lastname": "' + self.lastname + '", "firstname": "' + \
                      self.firstname + '", "birthday": "' + self.birthday.strftime(DATE_FORMAT_FRONT) + '", "height": ' + \
                      str(self.height) + ', "weight": ' + str(self.weight) + ', "flights": [' + json.dumps(flights_json) + ']}'
        return json_string


class Flight(Base):
    __tablename__ = 'flight'

    flight_id = Column(Integer, primary_key=True)
    flight_date = Column(Date)
    city_departure = Column(String(50))
    city_arrival = Column(String(50))
    data_csv = Column(Text)
    annotations = Column(String(50))
    pilot_id = Column(Integer, ForeignKey('pilot.pilot_id'))
    pilot = relationship("Pilot", back_populates="flights")
    exportations = relationship("Exportation", back_populates="flight")

    def __init__(self, flight_date, city_departure, city_arrival, data_csv, annotations, pilot_id):
        self.flight_date = flight_date
        self.city_departure = city_departure
        self.city_arrival = city_arrival
        self.data_csv = data_csv
        self.annotations = annotations
        self.pilot_id = pilot_id

    def __str__(self):
        return str(self.flight_id) + ", " + self.flight_date.strftime(DATE_FORMAT_FRONT) + ", " + self.city_departure + ", " + self.city_arrival + ", " + self.annotations + ", " + str(self.pilot_id)

    def to_json(self):
        json_string = '{"flight_id": ' + str(self.flight_id) + ', "flight_date": "' + self.flight_date.strftime(DATE_FORMAT_FRONT) + \
                      '", "city_departure": "' + self.city_departure + '", "city_arrival": "' + self.city_arrival + '", "data_csv": "' + self.data_csv + '", "annotations": "' + \
                      self.annotations + '", "pilot_id": ' + str(self.pilot_id) + '}'
        return json_string


class Exportation(Base):
    __tablename__ = 'exportation'

    exportation_id = Column(Integer, primary_key=True)
    label = Column(String(50))
    ts_start = Column(String(50))
    ts_end = Column(String(50))
    flight_id = Column(Integer, ForeignKey('flight.flight_id'))
    flight = relationship("Flight", back_populates="exportations")


if __name__ == '__main__':
    cherrypy_cors.install()
    config = {
        '/': {
            'cors.expose.on': True,
        },
        '/flight': {
            'cors.expose.on': True,
        },
        '/pipeline': {
            'cors.expose.on': True,
        },
        '/init': {
            'cors.expose.on': True,
        }
    }
    cherrypy.config.update({'server.socket_host': '0.0.0.0'})
    cherrypy.quickstart(Main(), config=config)
