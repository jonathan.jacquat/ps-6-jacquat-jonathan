import base64
import json
import neurokit2 as nk


def get_features(base64_data, start, end):
    string_array = base64_decoder(base64_data)
    array = json.loads(string_array)
    data = array[int(start):int(end)]
    processed_data, info = nk.ppg_process(ppg_signal=data, sampling_rate=64)
    features = nk.ppg_analyze(data=processed_data, sampling_rate=64)
    return processed_data["PPG_Clean"], features


def base64_decoder(base64_string):
    b64_bytes = base64_string.encode("utf-8")
    data_bytes = base64.b64decode(b64_bytes)
    return data_bytes.decode("utf-8")
